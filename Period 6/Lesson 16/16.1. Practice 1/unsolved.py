# İnteger tipləri roma rəqəminə çevirən klas yazın
# =================================================================

# =================================================================

# Roma rəqəmlərini integer tipinə çevirən klas yazın
# =================================================================

# =================================================================

# Elə klas yazın ki, cümlədə (), {} və [] mötərizələrinin doğru açılıb bağlandığını yoxlasın
# =================================================================

# =================================================================

# Verilmiş rəqəmlərin bütün kombinasiyasını qaytaran funksiya yazın
# Input : [4, 5, 6]
# Output : [[], [6], [5], [5, 6], [4], [4, 6], [4, 5], [4, 5, 6]]
# =================================================================

# =================================================================

# Elə klas yazın ki, iki parametr qəbul etsin: numbers və target. Numbers integer siyahı qəbul edir. Target isə tək integer qəbul edir. Siyahının içərisindən elə rəqəmlər seçin ki, onların cəmi target etsin. Və bu elementlərin indekslərini print etsin
# Input: numbers= [10,20,10,40,50,60,70], target=50
# Output: 3, 4
# =================================================================

# =================================================================

# Elə klas yazın ki, parametrdə integer siyahı qəbul etsin, daha sonra bu siyahıdan elə 3 rəqəm seçsin ki, onların cəmi sıfır etsin. Bütün bu üçlükləri siyahı kimi qaytarsın
# Input array : [-25, -10, -7, -3, 2, 4, 8, 10]
# Output : [[-10, 2, 8], [-7, -3, 10]]
# =================================================================

# =================================================================

# pow(x, n) funksiyasının gördüyü işi görən klas yazın
# =================================================================

# =================================================================

# Cümlədəki sözlərin hər birini tərsinə çevirən klas yazın
# =================================================================

# =================================================================

# Elə klas yazın ki, onun iki metodu olsun: 1. get_string metodu vasitəsilə istifadəçidən mətn daxil etməyi tələb etsin. 2. print_string vasitəsi ilə bu mətni böyük hərflərlə print etsin
# =================================================================

# =================================================================

# Rectangle adlı klas yaradın. Parametrdə düzbucaqlının uzunluğunu və hündürlüyünü qəbul etsin. Və compute_area metodu vasitəsi ilə bu düzbucaqlının sahəsini hesablasın qaytarsın
# =================================================================

# =================================================================

# Circle adlı klas yaradın. Parametrdə radius qəbul etsin. İki metodu olsun. Birinci metodu çevrənin uzunluğunu hesablasın. İkinci metod isə çevrənin sahəsini
# =================================================================

# =================================================================