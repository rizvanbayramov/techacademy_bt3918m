from array import array
import sys
from random import randint

# 5 elementli unsigned integer massivi yaradan və bütün elementlərini print edən proqram yazın. Massivin ayrı ayrı elementlərini indeksin köməkliyi ilə print edin
# =========================================================

# =========================================================

# massivin sonuna yeni element artıran proqram yazın
# =========================================================

# =========================================================

# massivin bütün elementlərini tərsinə çevirən proqram yazın
# =========================================================

# =========================================================


# Massivin tək elementinin ölçüsünü baytla göstərin
# =========================================================

# =========================================================

# massivin yaddaşdakı adresini və elementlərinin sayını print edin. Daha sonra elementlərin buferdə nə qədər yer tutduğunu print edin
# =========================================================

# =========================================================

# 3 rəqəminin massivdə neçə dəfə təkrarlandığını göstərin
# =========================================================

# =========================================================

# massivin sonuna aşağıdakı siyahını artırın və print edin
# =========================================================

# =========================================================

# massivi bytes massivinə çevirin
# =========================================================

# =========================================================

# xüsusi siyahını massivin sonuna artırın, lakin dəyərlər uyğun gəlmədikdə xəta mesajı çıxmasın
# =========================================================
mylist = [randint(0, 255) for i in range(1000)]
# =========================================================

# massivin indeksi 2 olan elementindən sonraya 300 rəqəmi əlavə edin
# =========================================================

# =========================================================

# massivin hər hansı bir elementini indeksinə görə silin
# =========================================================

# =========================================================

# massivin hər hansı bir elementini onun dəyərinə görə silin
# =========================================================

# =========================================================

# massivi adi siyahıya çevirin
# =========================================================

# =========================================================