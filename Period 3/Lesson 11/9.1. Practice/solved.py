#  Lüğəti elementlərin dəyərinə görə azalan və çoxalan sırada sıralayın
# ====================================================================
import operator
d = {1: 2, 3: 4, 4: 3, 2: 1, 0: 0}
print('Original dictionary : ',d)
sorted_d = sorted(d.items(), key=operator.itemgetter(0))
print('Dictionary in ascending order by value : ',sorted_d)
sorted_d = sorted(d.items(), key=operator.itemgetter(0),reverse=True)
print('Dictionary in descending order by value : ',sorted_d)

# ====================================================================

#  Lüğətə yeni element əlavə edin
# ====================================================================
d = {0:10, 1:20}
print(d)
d.update({2:30})
print(d)

# ====================================================================

#  lüğətlərin birləşmələrindən yeni lüğət yaradın
# ====================================================================
dic1={1:10, 2:20}
dic2={3:30, 4:40}
dic3={5:50,6:60}
dic4 = {}
for d in (dic1, dic2, dic3): dic4.update(d)
print(dic4)

# ====================================================================

#  verilmiş elementlərin lüğətin içərisində olub olmamasını yoxlayın
# ====================================================================
d = {1: 10, 2: 20, 3: 30, 4: 40, 5: 50, 6: 60}
a = 5
b = 9
print(a in d)
print(b in d)

# ====================================================================

#  lüğəti sadalayın, açar sözlərini və dəyərləri print edin
# ====================================================================
d = {'x': 10, 'y': 20, 'z': 30} 
for dict_key, dict_value in d.items():
    print(dict_key,'->',dict_value)
	
# ====================================================================

#  input vasitəsi ilə 1 və n aralığında rəqəm alın və onun əsasında lüğət yaradın. Lüğətin elementlərinin açar sözləri x, dəyəri isə x**2 olmalıdır.
# ====================================================================
n=int(input("Input a number "))
d = dict()

for x in range(1,n+1):
    d[x]=x*x

print(d) 

# ====================================================================

#  açar sözləri 1-dən 15-ə qədər olan rəqəmlər, dəyərləri isə açar sözün kvadratı olan lüğət yaradın
# ====================================================================
d=dict()
for x in range(1,16):
    d[x]=x**2
print(d)  
# ====================================================================

#  İki lüğəti birləşdirin (concatenate) 
# ====================================================================
d1 = {'a': 100, 'b': 200}
d2 = {'x': 300, 'y': 200}
d = d1.copy()
d.update(d2)
print(d)

# ====================================================================

#  Lüğəti sadalayın və items metodundan istifadə etmədən onun açar sözünü və dəyərini print edin
# ====================================================================
d = {'Red': 1, 'Green': 2, 'Blue': 3} 
for key in d:
     print(key, 'corresponds to ', d[key]) 

# ====================================================================

#  lüğətin içərisindəki bütün rəqəmlərin cəmini tapın
# ====================================================================
my_dict = {'data1':100,'data2':-54,'data3':247}
print(sum(my_dict.values()))
# ====================================================================

#  Lüğətin içərisindəki bütün rəqəmlərin hasilini tapın
# ====================================================================
my_dict = {'data1':100,'data2':-54,'data3':247}
result=1
for key in my_dict:    
    result=result * my_dict[key]

print(result)

# ====================================================================

#  Lüğətdən verilmiş açar sözə görə elementi silin
# ====================================================================
myDict = {'a':1,'b':2,'c':3,'d':4}
print(myDict)
if 'a' in myDict: 
    del myDict['a']
print(myDict)

# ====================================================================

#  İki siyahının uyğun elementlərindən yeni lüğət yaradın
# ====================================================================
keys = ['red', 'green', 'blue']
values = ['#FF0000','#008000', '#0000FF']
color_dictionary = dict(zip(keys, values))
print(color_dictionary)

# ====================================================================

#  Siyahını açar sözə görə sıralayın
# ====================================================================
color_dict = {'red':'#FF0000',
          'green':'#008000',
          'black':'#000000',
          'white':'#FFFFFF'}

for key in sorted(color_dict):
    print("%s: %s" % (key, color_dict[key]))
	
# ====================================================================

#  Lüğətin içərisindən maximum və minimumu tapın
# ====================================================================
my_dict = {'x':500, 'y':5874, 'z': 560}

maximum = max(my_dict.values())
minimum = min(my_dict.values())

print('Maximum Value: ',maximum)
print('Minimum Value: ',minimum)

# ====================================================================

#  Siyahıdan təkrarlanan elementləri silin
# ====================================================================
student_data = {'id1': 
   {'name': ['Sara'], 
    'class': ['V'], 
    'subject_integration': ['english, math, science']
   },
 'id2': 
  {'name': ['David'], 
    'class': ['V'], 
    'subject_integration': ['english, math, science']
   },
 'id3': 
    {'name': ['Sara'], 
    'class': ['V'], 
    'subject_integration': ['english, math, science']
   },
 'id4': 
   {'name': ['Surya'], 
    'class': ['V'], 
    'subject_integration': ['english, math, science']
   },
}

result = {}

for key,value in student_data.items():
    if value not in result.values():
        result[key] = value

print(result)

# ====================================================================

#  Lüğətin boş olub olmamasını yoxlayın
# ====================================================================
my_dict = {}

if not bool(my_dict):
    print("Dictionary is empty")
	
# ====================================================================

#  Lüğətin unikal olan elementlərini print edin
# ====================================================================
L = [{"V":"S001"}, {"V": "S002"}, {"VI": "S001"}, {"VI": "S005"}, {"VII":"S005"}, {"V":"S009"},{"VIII":"S007"}]
print("Original List: ",L)
u_value = set( val for dic in L for val in dic.values())
print("Unique Values: ",u_value)

# ====================================================================

#  Lüğəti cədvəl formatında print edin
# ====================================================================
my_dict = {'C1':[1,2,3],'C2':[5,6,7],'C3':[9,10,11]}
for row in zip(*([key] + (value) for key, value in sorted(my_dict.items()))):
    print(*row)

# ====================================================================

#  Lüğətin içərisində açar sözləri digər elementin dəyəri olan bütün elementlərin sayını tapın
# ====================================================================
student = [{'id': 1, 'success': True, 'name': 'Lary'},
 {'id': 2, 'success': False, 'name': 'Rabi'},
 {'id': 3, 'success': True, 'name': 'Alex'}]
print(sum(d['success'] for d in student))

# ====================================================================

#  Açar sözləri siyahının elementləri olan və iç-içə olan siyahılar yaradın
# ====================================================================
num_list = [1, 2, 3, 4]
new_dict = current = {}
for name in num_list:
    current[name] = {}
    current = current[name]
print(new_dict)

# ====================================================================

#  Siyahını əlifba sırasına görə sıralayan kod yazın
# ====================================================================
num = {'n1': [2, 3, 1], 'n2': [5, 1, 2], 'n3': [3, 2, 4]}
sorted_dict = {x: sorted(y) for x, y in num.items()}
print(sorted_dict)

# ====================================================================

#  Siyahının açar sözlərindən space-ləri silən kod yazın
# ====================================================================
student_list = {'S  001': ['Math', 'Science'], 'S    002': ['Math', 'English']}
print("Original dictionary: ",student_list)
student_dict = {x.translate({32: None}): y for x, y in student_list.items()}
print("New dictionary: ",student_dict)

# ====================================================================

#  Açar sözü, dəyəri və sıra sayını cədvəl formatında yazın
# ----------------------------
# nəticə belə olmalıdır
# ----------------------------
# key  value  count
# 1     10      1
# 2     20      2
# 3     30      3
# 4     40      4
# 5     50      5
# 6     60      6
# ----------------------------
# ====================================================================
dict_num = {1: 10, 2: 20, 3: 30, 4: 40, 5: 50, 6: 60}
print("key  value  count")
for count, (key, value) in enumerate(dict_num.items(), 1):
    print(key,'   ',value,'    ', count)

# ====================================================================

#  Lüğətin elementlərini sətir-sətir print edin
# ====================================================================
students = {'Aex':{'class':'V',
        'rolld_id':2},
        'Puja':{'class':'V',
        'roll_id':3}}
for a in students:
    print(a)
    for b in students[a]:
        print (b,':',students[a][b])
		
# ====================================================================

#  Müqayisə operatorlarının köməkliyi ilə lüğətin içərisində açar sözlərin olub olmamasını yoxlayın
# ====================================================================
student = {
  'name': 'Alex',
  'class': 'V',
  'roll_id': '2'
}
print(student.keys() >= {'class', 'name'})
print(student.keys() >= {'name', 'Alex'})
print(student.keys() >= {'roll_id', 'name'})

# ====================================================================

#  Hər iki lüğətdə olan elementləri print edin
# ====================================================================
x = {'key1': 1, 'key2': 3, 'key3': 2}
y = {'key1': 1, 'key2': 2}
for (key, value) in set(x.items()) & set(y.items()):
    print('%s: %s is present in both x and y' % (key, value))
# ====================================================================
